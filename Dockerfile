FROM jupyterhub/k8s-hub:2.0.0

USER 0
RUN pip3 install --proxy=http://proxy.ethz.ch:3128 kubernetes

USER 1000
CMD ["jupyterhub", "--config", "/usr/local/etc/jupyterhub/jupyterhub_config.py"]
